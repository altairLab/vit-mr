From Bitbucket
## VIT-MR

Virtual Industrial Task Master-slave Robot

VIT-MR (Virtual Industrial Task Master-slave Robot) dataset, was created to replicate all the steps of robotic
assisted tele-operated manipulation process typical in high precision small-scale manufacturing.

Please do not hesitate to contact us. We will send you the link to the mirror site for the download.
giovanni.menegozzo@univr.it